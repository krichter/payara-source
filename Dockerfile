FROM maven:slim
RUN apt-get update && apt-get install --yes git
RUN git clone --recurse-submodules https://github.com/payara/Payara
RUN cd Payara && mvn --batch-mode install && cd .. && unzip Payara/appserver/distributions/payara/target/payara.zip
RUN cd payara5 && bin/asadmin start-domain && cd ..
